import React,{useState} from 'react'
import Ongoing from './components/ongoing';
import Sessions from './components/sessions';
import ViewAll from './components/viewAll';
import './App.css';
const App = ()=>{
  const [onGoingSessionsData] = useState([{'subject':'MAT','topic':'Compound Algebra and Quadratic Equation','time':'Started at 4:00 pm','teacher':'Kiran Sir'},])
  const [upComingSessionsData] = useState([{'subject':'MAT','topic':'Functions','time':'28th June at 9:00 am','teacher':'Kiran Sir'},{'subject':'CHE','topic':'Mole Concept','time':'28th June at 9:00 am','teacher':'Srujan Sir'},{'subject':'PHY','topic':'Kinetics','time':'28th June at 9:00 am','teacher':'Laxman Sir'},{'subject':'MAT','topic':'Functions','time':'28th June at 9:00 am','teacher':'Kiran Sir'}])
  const [completedSessionsData] = useState([{'subject':'MAT','topic':'Quadratic Equation','time':'20th June at 9:00 am','teacher':'Kiran Sir'},{'subject':'CHE','topic':'Atomic Structure','time':'20th June at 9:00 am','teacher':'Srujan Sir'},{'subject':'PHY','topic':'Rotational Motion','time':'20th June at 9:00 am','teacher':'Laxman Sir'}]) 
  const [viewAllData,setViewAllData] = useState({'show':false,'title':''})
  const closeViewAll = ()=>setViewAllData({'show':false,'title':''})
  const showViewAll = title =>setViewAllData({'show':true,'title':title})
  return (
    <div className="container">
      {!viewAllData.show ? <React.Fragment>
                        <p className="paragraph">Live</p>
                        <Ongoing data={onGoingSessionsData}/>
                        <Sessions title="Upcoming" data={upComingSessionsData} expandViewAllRef = {showViewAll}/>
                        <Sessions title="Completed" data={completedSessionsData} expandViewAllRef = {showViewAll}/>
                      </React.Fragment>
                    :null}
      {viewAllData.show ? <ViewAll closeViewAllRef = {closeViewAll} title={viewAllData.title} data={viewAllData.title === "Upcoming" ? upComingSessionsData : completedSessionsData}/> : null}
    </div>
  )
}
export default App;