import './viewAll.css';
import DropdownIcon from '../images/dropdown.svg';
import BackIcon from '../images/back.svg'
import Image from '../images/img1.jpg';
import Colors from '../colors';
const ViewAll = props =>{
    return (
        <div className="viewall">
                <div className="heading">
                    <img src={BackIcon} alt="back" onClick={()=>props.closeViewAllRef()}/>
                    <p>{props.title}</p>
                </div>
            {props.data.map(dataItem=>{
                    return (
                        <div className="viewall-box">
                            <div className="session">
                                <img src={Image} alt="lectureimg" />
                                <div className="session-content">
                                    <p className="subject" style={{'color':Colors[dataItem.subject]}}>{dataItem.subject}</p>
                                    <p className="topic">{dataItem.topic}</p>
                                    <p className="time">{dataItem.time}</p>
                                    <p className="teacher">{dataItem.teacher}</p>
                                </div>
                            </div>
                            <div className="session-info">
                                {props.title === "Upcoming" ?<p>Join Class</p> : <p className="active">Watch Recorded Video</p>}
                                <img src={DropdownIcon} alt="back"/>
                            </div>
                        </div>
                    )
                })}
        </div>
    )
}
export default ViewAll;