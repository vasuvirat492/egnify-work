import './ongoing.css';
import Image from '../images/img1.jpg';
import Colors from '../colors'
const Sessions = props=>{
    return (
        <div className="box">
            <div className="box-heading">
                <p className="title">{props.title}</p>
                <p className="view-all-banner" onClick = {()=>props.expandViewAllRef(props.title)}>VIEW ALL</p>
            </div>
            <div className="session-container">
                {props.data.map(dataItem=>{
                    return (
                        <div className="session">
                            <img src={Image} alt="lectureimg" />
                            <div className="session-content">
                                <p className="subject" style={{'color':Colors[dataItem.subject]}}>{dataItem.subject}</p>
                                <p className="topic">{dataItem.topic}</p>
                                <p className="time">{dataItem.time}</p>
                                <p className="teacher">{dataItem.teacher}</p>
                            </div>
                        </div>
                    )
                })}
            </div>
        </div>
    )
} 
export default Sessions;